<?php

namespace Sergomet\DigiStorage;

use Exception;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;

class DigiStorage
{

    private $baseApi = 'https://storage.rcs-rds.ro';

    private $config;

    public function __construct(array $config)
    {
        $this->config = Config::get('digistorage');
    }

    public function auth()
    {
        $res = Http::post($this->baseApi . '/token', [
            'email' => $this->config['email'],
            'password' => $this->config['password'],
        ]);

        if ($res->status() !== 200) {
            throw new Exception('invalid credentials');
        }

        $data = json_decode($res->body(), true);

        return $data['token'];
    }

    public function token(): string
    {

        if (Cache::has('digi.token')) {
            $token = Cache::get('digi.token');
        } else {
            $token = $this->auth();
            Cache::forever('digi.token', $token);
        }
        return $token;
    }

    public function getMounts(): ?array
    {
        $url = $this->baseApi . '/api/v2/mounts?type=device,export';
        $res = $this->http()->get($url);

        return json_decode($res->body(), true);
    }

    public function getMountByName(string $name): ?array
    {
        $mounts = $this->getMounts();
        $mount = null;

        if (!$mounts['mounts']) {
            return null;
        }

        foreach ($mounts['mounts'] as $item) {
            if ($item['name'] === $name) {
                $mount = $item;
            }
        }

        return $mount;

    }

    public function getMountDigiCloud(): ?array
    {
        return $this->getMountByName($this->config['mount_name']);
    }

    public function getFile(string $filename): string
    {

        $url = $this->baseApi . '/api/v2/mounts/' . $this->getMount()->id . '/files/get?path=/' . $filename;
        $res = $this->http()->get($url);

        return $res->body();

    }

    public function store(string $filepath, string $filename)
    {

        $res = $this->http()
            ->attach(
                'attachment', file_get_contents($filepath), $filename
            )->post($this->baseApi . '/content/api/v2/mounts/' . $this->getMount()->id . '/files/put?path=/');

        return $res->body();

    }

    public function createDir(string $name)
    {
        $url = $this->baseApi . '/api/v2/mounts/' . $this->getMount()->id . '/files/folder?path=/';
        $response = $this->http()
            ->post($url, compact('name'));
    }

    private function getMount(): object
    {
        if (Cache::has('digi.mount_id')) {
            $mount = Cache::get('digi.mount_id');
        } else {
            $mount = $this->getMountDigiCloud();
            Cache::forever('digi.mount_id', $mount);
        }
        return (object) $mount;
    }

    private function http()
    {
        return Http::withHeaders([
            'Authorization' => 'Token token="' . $this->token() . '"',
        ]);
    }

}
